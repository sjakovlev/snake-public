/** @file snake_field.c
 *
 * Snake field (implementation)
 *
 * @author Sergei Jakovlev
 * @author Anton Prokopov
 */

#include <stdint.h>
#include <stdlib.h> // rand()
#include "snake_display.h"
#include "snake_field.h"


/*
 * Constants
 */


/**
 * Length of the array used to store segments of the snake
 *
 * Must be strictly greater than #SNAKE_MAX_LEN.
 */
#define SNAKE_ARR_LEN (SNAKE_MAX_LEN+1)



/**
 * Array that holds coordinates of snake segments
 *
 * Snakes head is defined by segment at #snakeHead, tail by #snakeTail.
 * If #snakeHead=3 and #snakeTail=6, segments 3, 4 and 5 should
 * be shown (and used for collision detection). If snakeHead >= snakeTail,
 * snake should wrap around and continue from start of the array.
 *
 * ### Example
 * @code
 * snakeSegments = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}, {0, 5}, ...};
 * snakeHead = 1;
 * snakeTail = 5;
 * @endcode
 *
 * In this situation, snake is 4 segment long. It's head is located at (0, 1).
 * Head segment is snakeSegments[1]. Second segment is located at (0, 2),
 * third at (0, 3) and fourth at (0, 4).
 *
 * @code
 * SNAKE_MAX_LEN = 5
 * snakeSegments = {{0, 0}, {0, 1}, {0, 2}, {0, 3}, {0, 4}};
 * snakeHead = 3;
 * snakeTail = 2;
 * @endcode
 *
 * In this situation active segments are 3, 4, 0, 1 (in that order).
 */
static uint8_t snakeSegments[SNAKE_ARR_LEN][2];


/*
 * Global variables
 */


/**
 * Index of a segment corresponding to the head of the snake
 */
volatile uint8_t snakeHead = 0;

/**
 * Index of a segment corresponding to the tail (end) of the snake.
 *
 * This is the index of segment after the last one that should be displayed.
 * That means, if snakeHead=3 and snakeTail=6, segments 3, 4 and 5 should
 * be shown (and used for collision detection).
 */
volatile uint8_t snakeTail = 0;



/**
 * Width of the field
 */
volatile uint8_t snakeFieldWidth;

/**
 * Height of the field
 */
volatile uint8_t snakeFieldHeight;

/**
 * Equal to #snakeFieldWidth * #snakeFieldHeight
 */
volatile uint8_t snakeFieldSize;



/**
 * Defines whether field is horizontally and vertically wrapped
 *
 * If the value is 0, collisions with borders end a game; otherwise snake
 * wraps to the other side of the screen.
 */
volatile uint8_t snakeIsFieldWrapped;



/**
 * x coordinate of randomly spawned food
 */
volatile uint8_t foodX;

/**
 * y coordinate of randomly spawned food
 */
volatile uint8_t foodY;


/*
 * Functions
 */


void snakeFieldInit(uint8_t fieldWidth, uint8_t fieldHeight) {

	snakeFieldWidth = fieldWidth;
	snakeFieldHeight = fieldHeight;

	snakeFieldSize = snakeFieldWidth * snakeFieldHeight;

	snakeDisplayInit(fieldWidth, fieldHeight);
}



void snakeInit(uint8_t x, uint8_t y) {

	snakeHead = 0;
	snakeTail = 1;

	snakeSegments[snakeHead][0] = x;
	snakeSegments[snakeHead][1] = y;

	// Clear display and draw new head
	snakeDisplayClear();
	snakeDisplayPutPixel(x, y);
}



void snakeSetIsFieldWrapped(uint8_t isWrapped) {
	snakeIsFieldWrapped = isWrapped;
}



uint8_t snakeGetIsFieldWrapped() {
	return snakeIsFieldWrapped;
}



void snakeRedrawAll() {

	snakeDisplayClear();

	uint8_t curIdx = snakeHead;

	while (curIdx != snakeTail) {

		uint8_t curX = snakeSegments[curIdx][0];
		uint8_t curY = snakeSegments[curIdx][1];

		snakeDisplayPutPixel(curX, curY);

		if (curIdx < SNAKE_ARR_LEN - 1) {
			curIdx++;
		} else {
			curIdx = 0;
		}
	}
}



/**
 * Move snake's head one step in the given direction
 *
 * Depending on the value of moveTail argument, can either "drag" the body or
 * make it longer.
 *
 * @param direction one of #SNAKE_UP, #SNAKE_DOWN, #SNAKE_LEFT or #SNAKE_RIGHT
 * @param moveTail if 0, increase snake's length; remove last segment otherwise
 *
 * @return 0 if snake collided with field borders or itself; 1 otherwise
 *
 * @see snakeStep()
 * @see snakeEat()
 */
static uint8_t snakeStepGeneric(uint8_t direction, uint8_t moveTail) {

	// Update head coordinates

	uint8_t x = snakeSegments[snakeHead][0];
	uint8_t y = snakeSegments[snakeHead][1];

	if (direction == SNAKE_DOWN) {
		y++;

		if (snakeIsFieldWrapped && y >= snakeFieldHeight) {
			y = 0;
		}
	} else if (direction == SNAKE_UP) {
		y--;

		if (snakeIsFieldWrapped && y >= snakeFieldHeight) { // y is unsigned!
			y = snakeFieldHeight - 1;
		}
	} else if (direction == SNAKE_LEFT) {
		x--;

		if (snakeIsFieldWrapped && x >= snakeFieldWidth) { // x is unsigned!
			x = snakeFieldWidth - 1;
		}
	} else if (direction == SNAKE_RIGHT) {
		x++;

		if (snakeIsFieldWrapped && x >= snakeFieldWidth) {
			x = 0;
		}
	}

	// Move head

	if (snakeHead > 0) {
		snakeHead--;
	} else {
		snakeHead = SNAKE_ARR_LEN - 1;
	}

	// Store new head coordinates

	snakeSegments[snakeHead][0] = x;
	snakeSegments[snakeHead][1] = y;

	// Remove tail if needed

	if (moveTail || snakeHead == snakeTail) {
		if (snakeTail > 0) {
			snakeTail--;
		} else {
			snakeTail = SNAKE_ARR_LEN - 1;
		}

		snakeDisplayRemovePixel(
				snakeSegments[snakeTail][0],
				snakeSegments[snakeTail][1]);
	}

	// Draw new segment head on screen

		snakeDisplayPutPixel(x, y);

	// Check if snake collides with borders or itself

	// Since we are using uint8_t, if snake reaches left or top borders, value
	// cannot be negative (happens overflow).
	if (x >= snakeFieldWidth || y >= snakeFieldHeight) {
		// Collision with border (left the field)
		return 1;
	}

	uint8_t curIdx = snakeHead;

	while (curIdx != snakeTail) {
		uint8_t curX = snakeSegments[curIdx][0];
		uint8_t curY = snakeSegments[curIdx][1];

		if (curIdx != snakeHead && curX == x && curY == y) {
			// Collision with itself
			return 1;
		}

		if (curIdx < SNAKE_ARR_LEN - 1) {
			curIdx++;
		} else {
			curIdx = 0;
		}
	}

	return 0;
}

uint8_t snakeStep(uint8_t direction) {
	return snakeStepGeneric(direction, 1);
}

uint8_t snakeEat(uint8_t direction) {
	return snakeStepGeneric(direction, 0);
}



uint8_t snakeIsPositionTaken(uint8_t x, uint8_t y) {
	uint8_t curIdx = snakeHead;

	while (curIdx != snakeTail) {
		uint8_t curX = snakeSegments[curIdx][0];
		uint8_t curY = snakeSegments[curIdx][1];

		if (curX == x && curY == y) {
			return 1;
		}

		if (curIdx < SNAKE_ARR_LEN - 1) {
			curIdx++;
		} else {
			curIdx = 0;
		}
	}

	return 0;
}



uint8_t spawnFood() {

	uint8_t foodDisplacement = rand() % (snakeFieldSize - getSnakeLength());

	uint8_t x = 0;
	uint8_t y = 0;

	while(1) {
		if (!snakeIsPositionTaken(x, y)) {

			if (!foodDisplacement) {
				break;
			}

			foodDisplacement--;
		}

		if (x >= snakeFieldWidth - 1) {
			x = 0;
			y++;
		} else {
			x++;
		}

		if (y >= snakeFieldHeight) {
			return 0;
		}
	};

	foodX = x;
	foodY = y;

	snakeDisplayPutPixel(foodX, foodY);

	return 1;
}



uint8_t getFoodX() {
	return foodX;
}

uint8_t getFoodY() {
	return foodY;
}

uint8_t getHeadX() {
	return snakeSegments[snakeHead][0];
}

uint8_t getHeadY() {
	return snakeSegments[snakeHead][1];
}

uint8_t getSnakeLength() {
	if (snakeTail >= snakeHead) {
		return snakeTail - snakeHead;
	} else {
		return snakeTail + (SNAKE_ARR_LEN - snakeHead);
	}
}
