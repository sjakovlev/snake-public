#include <snake_display.h>
#include <stdint.h>

#include "lcd_io.h"


/**
 * Custom symbols used by this module to display the snake
 */
static const uint8_t snakeSymbols[8][8] = {
		{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // 000
		{0x00, 0x00, 0x00, 0x00, 0x0e, 0x1f, 0x1f, 0x0e}, // 001
		{0x0e, 0x1f, 0x1f, 0x0e, 0x00, 0x00, 0x00, 0x00}, // 010
		{0x0e, 0x1f, 0x1f, 0x0e, 0x0e, 0x1f, 0x1f, 0x0e}, // 011

		// Reserved
		{0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}, // 100
		{0x00, 0x00, 0x00, 0x00, 0x1f, 0x1f, 0x1f, 0x1f}, // 101
		{0x1f, 0x1f, 0x1f, 0x1f, 0x00, 0x00, 0x00, 0x00}, // 110
		{0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f, 0x1f}  // 111
};

static const char SNAKE_SYM_TOP = 0x02;
static const char SNAKE_SYM_BOTTOM = 0x01;


static uint8_t snakeLcdDataBuffer[2][16];


void snakeDisplayInit(uint8_t width, uint8_t height) {

	// Initialize LCD

	lcdInit();
	lcdCursorOff();
	lcdBlinkerOff();

	// Check that right parameters were passed, show error message otherwise

	if (width != 16 || height != 4) {
		lcdLine1();
		lcdWriteSz("Error: only 16x4");
		lcdLine2();
		lcdWriteSz("field supported!");
		return;
	}

	// Load symbols to LCD

	for (uint8_t i = 0; i < 8; i++) {
		lcdSaveSymbol(i, snakeSymbols[i]);
	}
}

void snakeDisplayPutPixel(uint8_t x, uint8_t y) {

	uint8_t line = y / 2;
	uint8_t isBottom = y - line * 2;

	snakeLcdDataBuffer[line][x] |= isBottom ? SNAKE_SYM_BOTTOM : SNAKE_SYM_TOP;

	lcdCursorToPosition(line, x);
	lcdWriteChar(snakeLcdDataBuffer[line][x]);
}

void snakeDisplayRemovePixel(uint8_t x, uint8_t y) {

	uint8_t line = y / 2;
	uint8_t isBottom = y - line * 2;
	
	snakeLcdDataBuffer[line][x] &= ~(isBottom ? SNAKE_SYM_BOTTOM : SNAKE_SYM_TOP);

	lcdCursorToPosition(line, x);
	lcdWriteChar(snakeLcdDataBuffer[line][x]);
}

void snakeDisplayClear() {

	for (uint8_t i = 0; i < 16; ++i) {
		snakeLcdDataBuffer[0][i] = 0;
		snakeLcdDataBuffer[1][i] = 0;
	}

	lcdClear();
}
