/** @file snake_main.c
 *
 * Main file of the Snake project (implementation)
 *
 * @author Sergei Jakovlev
 * @author Anton Prokopov
 */

#include <stdlib.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include "lcd_io.h"
#include "snake_field.h"
#include "snake_display.h"

#include "snake_main.h"


/**
 * Constants (local)
 *
 * Global defines are in the header file.
 */


/**
 * Screen width
 */
static const uint8_t SCREEN_WIDTH  = 16;

/**
 * Screen height
 */
static const uint8_t SCREEN_HEIGHT = BUTTON_DOWN;



/**
 * Timer delay used when showing start screen
 */
static const uint8_t TIMER_DELAY_START_SCREEN = 70;

/**
 * Base of timer delay used when playing
 *
 * With each snake length increase delay will go down by
 * #TIMER_DELAY_STARTED_STEP until reaching #TIMER_DELAY_STARTED_MIN, which will
 * be the smallest delay value (fastest snake's speed) used.
 *
 * @see #TIMER_DELAY_STARTED_STEP
 * @see #TIMER_DELAY_STARTED_MIN
 */
static const uint8_t TIMER_DELAY_STARTED_BASE = 150;

/**
 * Value by which delay will be decreased with each eaten food when playing
 *
 * @see #TIMER_DELAY_STARTED_BASE
 * @see #TIMER_DELAY_STARTED_MIN
 */
static const uint8_t TIMER_DELAY_STARTED_STEP = BUTTON_DOWN;

/**
 * Minimal delay value (fastest speed) used when playing
 *
 * @see #TIMER_DELAY_STARTED_BASE
 * @see #TIMER_DELAY_STARTED_STEP
 */
static const uint8_t TIMER_DELAY_STARTED_MIN = 20;

/**
 * Timer delay used when showing end screen (win or loss)
 */
static const uint8_t TIMER_DELAY_OVER = 100;


/**
 * Length of message displayed on start screen
 */
static const uint8_t START_SCREEN_MESSAGE_LENGTH = 65;


/**
 * Message displayed on start screen
 */
static const char START_SCREEN_MESSAGE[] =
		" Press CENTER to start  *  Anton Prokopov  *  Sergei Jakovlev  * ";


/**
 * This string will be displayed on top of start screen in wrapped mode
 */
static const char START_SCREEN_NAME_WRAPPED[] = "SNAKE (wrapped) ";


/**
 * This string will be displayed on top of start screen in hardcore mode
 */
static const char START_SCREEN_NAME_HARDCORE[] = "SNAKE (hardcore)";


/**
 * Length of "Konami code" used by this game
 *
 * @see #konamiCode
 */
static uint8_t KONAMI_CODE_LENGTH = BUTTON_CENTER;


/**
 * "Konami code" combination used by this game
 *
 * It is simpler than the real Konami code, because the input is really hard.
 * Cheat to win: left, right, left, right, center, center.
 */
static uint8_t konamiCode[] = { BUTTON_LEFT, BUTTON_RIGHT, BUTTON_LEFT,
		BUTTON_RIGHT, BUTTON_CENTER, BUTTON_CENTER };


/*
 * Global variables
 */


/**
 * Direction in which snake should move on next game state update
 */
volatile uint8_t direction;

/**
 * Direction in which snake moved during last game state update
 */
volatile uint8_t directionLast;

/**
 * One if center button was pressed after last game state update
 */
volatile uint8_t centerPressed;

/**
 * One if snake has eaten and should grow on next game state update
 */
volatile uint8_t growOnNextStep;

/**
 * Game state
 *
 * Should be #GAME_STATE_NEW, #GAME_STATE_STARTED or #GAME_STATE_OVER.
 */
volatile uint8_t gameState;

/**
 * Seed used by srand() on game start
 *
 * Since we have no reliable real-time clock, we use this variable to generate
 * random seed. This value is increased by each button poll. It is used when
 * user finally starts the game.
 */
volatile uint8_t randomSeed;

/**
 * Contains last pressed button
 *
 * @see BUTTON_UP
 * @see BUTTON_DOWN
 * @see BUTTON_LEFT
 * @see BUTTON_RIGHT
 * @see BUTTON_CENTER
 */
volatile uint8_t buttonPressed;

/**
 * Users progress with entering Konami code
 *
 * Konami code will activate when this variable is equal to #KONAMI_CODE_LENGTH
 */
volatile uint8_t konamiIndex;

/**
 * String that is used to show the final score when game ends
 */
static char scoreLine[] = "   Score:  00";


/**
 * Shows how many times we can poll buttons without refreshing game state
 *
 * When this value is zero or greater than #BUTTON_POLLS_PER_GAME_UPDATE, next
 * button state poll will also cause game state update.
 */
volatile uint8_t buttonLoopsUntilGameLoop = BUTTON_POLLS_PER_GAME_UPDATE;

/**
 * Shows how many times we can poll buttons without checking Konami code
 *
 * When this value is zero or greater than #BUTTON_POLLS_KONAMI, next
 * button state poll will also cause game state update.
 */
volatile uint8_t buttonLoopsUntilKonamiLoop = BUTTON_POLLS_KONAMI;



/**
 * Current offset of start screen message
 */
volatile uint8_t startScreenMessageOffset;


/*
 * Interrupt handlers
 */


/**
 * Button polling and game update timer interrupt vector handler
 *
 * Polls buttons and updates  what will be done on next game frame update.
 *
 * If #buttonLoopsUntilKonamiLoop is zero or greater than
 * #BUTTON_POLLS_KONAMI, this handler also launches game state update
 * and resets #buttonLoopsUntilKonamiLoop to #BUTTON_POLLS_KONAMI.
 * Otherwise it **decreases** value of #buttonLoopsUntilKonamiLoop.
 *
 * The same goes for #buttonLoopsUntilGameLoop only with less frequency:
 * If #buttonLoopsUntilGameLoop is zero or greater than
 * #BUTTON_POLLS_PER_GAME_UPDATE, this handler also launches game state update
 * and resets #buttonLoopsUntilGameLoop to #BUTTON_POLLS_PER_GAME_UPDATE.
 * Otherwise it **decreases** value of #buttonLoopsUntilGameLoop.
 */
ISR(TIMER0_COMPA_vect) {

	randomSeed++;

	updateButtonState();

	if(gameState == GAME_STATE_STARTED){
		if (buttonLoopsUntilKonamiLoop == 0 ||
		buttonLoopsUntilKonamiLoop > BUTTON_POLLS_KONAMI) {
			buttonLoopsUntilKonamiLoop = BUTTON_POLLS_KONAMI;
			updateKonamiState();
		}
	}

	buttonLoopsUntilKonamiLoop--;

	if (buttonLoopsUntilGameLoop == 0 ||
			buttonLoopsUntilGameLoop > BUTTON_POLLS_PER_GAME_UPDATE) {
		buttonLoopsUntilGameLoop = BUTTON_POLLS_PER_GAME_UPDATE;
		updateGameState();
	}

	buttonLoopsUntilGameLoop--;
}


/*
 * Static functions
 */


/**
 * Timer initialization
 */
static void initTimer() {
	TIMSK0 = (1<<OCIE0A);
	TCCR0A = (1<<WGM01); // Normal mode, clear on compare match
	TCCR0B = (1<<CS02) | (1<<CS00); //clk/1024
	OCR0A = TIMER_DELAY_START_SCREEN;
}


/**
 * Set timer/counter 0 delay and reset counter
 *
 * @param delay value that will be set as OCR0A
 */
static void setTimerDelay(uint8_t delay) {
	OCR0A = delay;
	TCNT0 = 0;
}


/**
 * Update player's score after every eaten point
 */
static void updateScoreLine() {
	uint8_t tenths = getSnakeLength() / 10;
	uint8_t units = getSnakeLength() - (tenths * 10);
	scoreLine[11] = tenths + '0';
	scoreLine[12] = units + '0';
}


/**
 * Update game state before round start
 */
static void updateGameStateStartScreen() {
	//lcdShiftDisplayLeft();

	lcdLine2();

	uint8_t arrayLoc = startScreenMessageOffset;

	for (uint8_t i = 0; i < SCREEN_WIDTH; ++i) {
		lcdWriteChar(START_SCREEN_MESSAGE[arrayLoc]);

		arrayLoc++;
		if (arrayLoc >= START_SCREEN_MESSAGE_LENGTH) {
			arrayLoc = 0;
		}
	}

	startScreenMessageOffset++;

	if (startScreenMessageOffset >= START_SCREEN_MESSAGE_LENGTH) {
		startScreenMessageOffset = 0;
	}

	if (centerPressed) {
		srand(randomSeed);
		startGame();
	}

	if (buttonPressed == BUTTON_LEFT) {
		snakeSetIsFieldWrapped(1);
		lcdLine1();
		lcdWriteSz(START_SCREEN_NAME_WRAPPED);
	}

	if (buttonPressed == BUTTON_RIGHT) {
		snakeSetIsFieldWrapped(0);
		lcdLine1();
		lcdWriteSz(START_SCREEN_NAME_HARDCORE);
	}

	buttonPressed = 0;
}


/**
 * Update game state during round
 */
static void updateGameStatePlaying() {

	int res;

	if (growOnNextStep) {
		res = snakeEat(direction);
		growOnNextStep = 0;
	} else {
		res = snakeStep(direction);
	}

	directionLast = direction;

	if (res != 0){
		stopGame();
		return;
	}

	if (getHeadX() == getFoodX() && getHeadY() == getFoodY()){
		if (!spawnFood()) {
			winGame();
		}
		growOnNextStep = 1;

		uint16_t delayReduction = getSnakeLength() * TIMER_DELAY_STARTED_STEP;

		if (delayReduction >= TIMER_DELAY_STARTED_BASE ||
				(TIMER_DELAY_STARTED_BASE - delayReduction) <
					TIMER_DELAY_STARTED_MIN) {
			setTimerDelay(TIMER_DELAY_STARTED_MIN);
		} else {
			setTimerDelay(TIMER_DELAY_STARTED_BASE - delayReduction);
		}
	}
}


/**
 * Update game state after game has ended
 */
static void updateGameStateOver() {

	if (centerPressed) {
		showStartScreen();
	}
}


/*
 * Functions
 */


void showStartScreen() {
	cli();

	lcdClear();
	lcdLine1();

	if (snakeGetIsFieldWrapped()) {
		lcdWriteSz(START_SCREEN_NAME_WRAPPED);
	} else {
		lcdWriteSz(START_SCREEN_NAME_HARDCORE);
	}

	gameState = GAME_STATE_NEW;

	setTimerDelay(TIMER_DELAY_START_SCREEN);

	sei();
}


void startGame() {
	cli();

	snakeFieldInit(SCREEN_WIDTH, SCREEN_HEIGHT);
	snakeInit(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
	spawnFood();

	direction = SNAKE_RIGHT;

	gameState = GAME_STATE_STARTED;

	setTimerDelay(TIMER_DELAY_STARTED_BASE);

	sei();
}


void stopGame() {
	cli();

	lcdClear();
	lcdLine1();
	lcdWriteSz("   GAME OVER!   ");
	lcdLine2();
	updateScoreLine();
	lcdWriteSz(scoreLine);

	gameState = GAME_STATE_OVER;

	setTimerDelay(TIMER_DELAY_OVER);

	konamiIndex = 0;

	sei();
}


void winGame() {
	cli();

	lcdClear();
	lcdLine1();
	lcdWriteSz("    YOU WIN!");
	lcdLine2();
	lcdWriteSz("Congratulations!");

	gameState = GAME_STATE_OVER;
	centerPressed = 0;

	sei();
}


void updateButtonState() {
	if (!(PINF & (1 << PINF7))) {
		buttonPressed = BUTTON_UP;
		if (directionLast != SNAKE_DOWN){
			direction = SNAKE_UP;
		}
	}

	if (!(PINF & (1 << PINF3))) {
		buttonPressed = BUTTON_RIGHT;
		if (directionLast != SNAKE_LEFT){
			direction = SNAKE_RIGHT;
		}
	}

	if (!(PINF & (1 << PINF5))) {
		buttonPressed = BUTTON_LEFT;
		if (directionLast != SNAKE_RIGHT){
			direction = SNAKE_LEFT;
		}
	}

	if (!(PINF & (1 << PINF4))) {
	buttonPressed = BUTTON_DOWN;
		if (directionLast != SNAKE_UP){
			direction = SNAKE_DOWN;
		}
	}

	if (!(PINF & (1 << PINF6))) {
	buttonPressed = BUTTON_CENTER;
		centerPressed = 1;
	}
}


void updateKonamiState(){
	if (konamiIndex >= KONAMI_CODE_LENGTH) {
		konamiIndex = 0;
		winGame();
	} else {
		if (buttonPressed == konamiCode[konamiIndex]) {
			konamiIndex++;
		} else if (buttonPressed != 0 && (konamiIndex < 1 ||
				buttonPressed != konamiCode[konamiIndex-1])) {
			konamiIndex = 0;
		}

		buttonPressed = 0;
	}
}


/*
 * See updateGameStateStartScreen(), updateGameStatePlaying() and
 * updateGameStateOver() to see concrete actions taken during update
 * in different states.
 */
void updateGameState() {
	switch (gameState) {
	case GAME_STATE_NEW:
		updateGameStateStartScreen();
		break;
	case GAME_STATE_STARTED:
		updateGameStatePlaying();
		break;
	case GAME_STATE_OVER:
		updateGameStateOver();
		break;
	default:
		showStartScreen();
		break;
	}

	centerPressed = 0;
}


int main() {

	// Initialization

	initTimer();

	lcdInit();
	lcdCursorOff();
	lcdBlinkerOff();
	
	// Joystick initialization

	MCUCR = (1<<JTD);
	MCUCR = (1<<JTD);
	DDRF = (1<<PF0) | (1<<PF1);
	PORTF = ~((1<<PF0) | (1<<PF1) | (1<<PF2));

	// Wrapped field by default
	snakeSetIsFieldWrapped(1);

	// Enable interrupts
	sei();

	// Show start screen
	showStartScreen();

	while(1){};
}
