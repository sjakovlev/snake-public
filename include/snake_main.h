/** @file snake_main.h
 *
 * Main file of the Snake project (implementation)
 *
 * @author Sergei Jakovlev
 * @author Anton Prokopov
 */

#ifndef _SNAKE_MAIN_H_
#define _SNAKE_MAIN_H_


/**
 * Constants
 */


/**
 * Game state will be updated once in every BUTTON_POLLS_PER_GAME_UPDATE polls
 */
#define BUTTON_POLLS_PER_GAME_UPDATE 10

/**
 * Konami code input will be checked once in every BUTTON_POLLS_KONAMI polls
 */
#define BUTTON_POLLS_KONAMI 5


/**
 * State when start screen is shown
 */
#define GAME_STATE_NEW     0

/**
 * State when game is started and user can play
 */
#define GAME_STATE_STARTED 1

/**
 * State after the game has ended (win or loss)
 */
#define GAME_STATE_OVER    2



/*
 * Button definitions
 */
#define BUTTON_UP     7  //!< Up button
#define BUTTON_RIGHT  3  //!< Right button
#define BUTTON_LEFT   5  //!< Left button
#define BUTTON_DOWN   4  //!< Down button
#define BUTTON_CENTER 6  //!< Center button


/*
 * Functions
 */


/**
 * Start game screen. This is where it all begins!
 */
void showStartScreen();


/**
 * Initialize field, snake and the first point and start moving
 */
void startGame();


/**
 * Show player game over screen in case of losing
 */
void stopGame();

/**
 * Show player win screen in case of winning the game
 */
void winGame();



/**
 * Poll states of buttons and select action taken on next game state update
 *
 * @see updateGameState()
 */
void updateButtonState();

/**
 * Update global state of the game
 *
 * This function uses values stored in global variables (that are modified by
 * updateButtonState() function) to perform player action, update game state
 * and draw it on screen (if needed).
 *
 * @see updateButtonState()
 */
void updateGameState();

/**
 * Update the index of 6-symbol konami combination to achieve an easy win
 */
void updateKonamiState();



/**
 * Initialize and launch project
 *
 * @return nothing, ends with endless loop
 */
int main();



#endif /* _SNAKE_MAIN_H_ */
