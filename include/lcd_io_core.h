/** @file lcd_io_core.h
 *
 * LCD IO library - core definitions and functions
 *
 * This file is part of LCD IO library for ST7066U display module
 * used in combination with custom AT90USB647 development board created by
 * University of Tartu and used in its "Microprocessors" course.
 *
 * This file contains definitions of physical pins, masks for convenient use
 * of instructions and core helper functions needed to use ST7066U LCD MCU.
 *
 * Note that if you want to also use convenient wrapper functions (writing
 * strings, etc), you should use **lcd_io** instead (lcd_io_core will also be
 * included automatically).
 *
 * **WARNING:** this implementation assumes that maximal MCU frequency is 16MHz.
 * Care should be taken when porting code to MCUs that work on higher
 * frequencies. See #LCD_E for details on timings.
 *
 * ### Prerequisites
 *
 * This library uses functions from <util/delay.h>, which requires correct
 * F_CPU preprocessor constant to be present in order provide correct delays.
 *
 * F_CPU is MCU clock speed in Hz as unsigned long (e.g. `2000000UL` for 2MHz).
 *
 * Additionally, compiler optimizations must be turned on for delay functions
 * to work correctly.
 *
 * ### Usage
 *
 * Display MCU must be initialized before any other instruction can be used.
 * This library provides convenience methods to simplify this process.
 *
 * 1. lcdInitPorts() sets up physical GPIO pins on development board and must be
 *    called first.
 * 2. Then, lcdInitInstr() must be called to send initialization sequence
 *    to display MCU. Note that this can work only if more than 40ms has passed
 *    from the moment when Vcc was applied to display module (during that time
 *    display MCU is booting up). lcdInitInstrWithDelay() can be used to ensure
 *    that this condition is met if initialization is performed immediately
 *    after reboot.
 *
 * #### Example
 *
 * @code
 * lcdInitPorts();
 * lcdInitInstrWithDelay();
 * // now other functions can be used
 * lcdWriteSz("Hello World!");
 * @endcode
 *
 * @author Sergei Jakovlev
 */

#ifndef _LCD_IO_CORE_H_
#define _LCD_IO_CORE_H_

/*
 * Check that required constant F_CPU is set
 *
 * F_CPU is MCU clock speed in Hz as unsigned long.
 */
#ifndef F_CPU
#error "F_CPU must be defined for lcd_io to work correctly"
#endif /* F_CPU */

#include <avr/io.h>
#include <util/delay.h> /* use provided delay functions */



/*************************
 * LCD pin configuration *
 *************************/

#define LCD_04    PD7    //!< LCD pin 4, see #LCD_RS for usage
#define LCD_05    PD6    //!< LCD pin 5, see #LCD_RW for usage
#define LCD_06    PE0    //!< LCD pin 6, see #LCD_E for usage
#define LCD_07    PC7    //!< LCD pin 7, see #LCD_DB0 for usage
#define LCD_08    PC6    //!< LCD pin 8, see #LCD_DB1 for usage
#define LCD_09    PC5    //!< LCD pin 9, see #LCD_DB2 for usage
#define LCD_10    PC4    //!< LCD pin 10, see #LCD_DB3 for usage
#define LCD_11    PC3    //!< LCD pin 11, see #LCD_DB4 for usage
#define LCD_12    PC2    //!< LCD pin 12, see #LCD_DB5 for usage
#define LCD_13    PC1    //!< LCD pin 13, see #LCD_DB6 for usage
#define LCD_14    PC0    //!< LCD pin 14, see #LCD_DB7 for usage

/**
 * LCD register selection (RS) pin
 *
 * Selects display register used by operations. Should always be set as output.
 *
 * ### States
 * * Low (0)
 *   * for write, instruction register is used
 *   * for read, busy flag address counter is used
 * * High (1)
 *   * for write and read, data register is used (either CGRAM or DDRAM)
 *
 * @see lcdWriteInstrRev() function sets LCD_RS to 0 when writing instruction
 * @see lcdWriteData() function sets LCD_RS to 1 when writing data
 * @see lcdWriteDataRev() function sets LCD_RS to 1 when writing data
 * @see lcdReadData() function sets LCD_RS to 1 when reading data
 * @see lcdReadDataRev() function sets LCD_RS to 1 when reading data
 * @see lcdWaitWhileBusy() function sets LCD_RS to 0 and reads busy flag
 * @see #LCD_RW for read/write mode selection
 */
#define LCD_RS LCD_04

/**
 * LCD select read/write pin
 *
 * Selects whether operation should read data from display MCU or write it
 * to display MCU using data bus pins (#LCD_DB7 - #LCD_DB0).
 * Should always be set as output.
 *
 * Note that #LCD_RS, #LCD_RW and #LCD_E are not affected by the state
 * of this pin and are always used as output.
 *
 * ### States
 * * Low (0) - data bus is used to write data to display MCU.
 *   Note that DB pins must be set to output mode (#LCD_DB_DDR = 0xff).
 * * High (1) - data bus is used to read data from display MCU.
 *   Note that DB pins must be set to input mode with pull-up resistor enabled
 *   (#LCD_DB_DDR = 0x00; #LCD_DB_PORT = 0xff;).
 *
 * @see lcdWriteInstrRev() function sets RW to 0 when writing instruction
 * @see lcdWriteData() function sets RW to 0 when writing data
 * @see lcdWriteDataRev() function sets RW to 0 when writing data
 * @see lcdReadData() function sets RW to 1 when writing data
 * @see lcdReadDataRev() function sets RW to 1 when writing data
 * @see lcdWaitWhileBusy() function sets RW to 1 and reads busy flag
 * @see #LCD_RS for register selection
 */
#define LCD_RW LCD_05

/**
 * Start data read/write pin
 *
 * Controls display MCU operation. Data is transmitted between main board and
 * display MCU when LCD_E is strobed (set to high, then low).
 *
 * States:
 * * Low (0) - stop read/write.
 *
 *   Pin should be set to this state after instruction or data write/read
 *   operation is completed. If not, subsequent operations may fail.
 *
 * * High (1) - start read/write.
 *
 *   Pin should be set to this state before instruction or data write/read
 *   operation is initiated.
 *
 * ### Timings
 *
 * LCD_E rise/fall time can be up to 25ns (Tr, Tf).
 *
 * When writing data, 40ns delay is required before LCD_E is set to 0
 * after data pins are set (Tdsw, data setup time).
 *
 * When reading data, 100ns delay is required after LCD_E is set
 * and before data is read (Tddr, data setup time).
 *
 * Minimal LCD_E pulse width (time between E=1 and E=0) is 140ns
 * (Tpw, enable pulse width) and minimal cycle width (time between two
 * subsequent operations that set LCD_E to 1) is 1200ns (Tc, enable cycle time).
 *
 * #LCD_RS, #LCD_RW and #LCD_DB7-#LCD_DB0 pins must retain their state
 * for at least 10ns after LCD_E is set to 0 (Tah, address hold time, and Th,
 * data hold time).
 *
 * See display documentation for details on required timings.
 *
 * ### Examples
 *
 * Writing data:
 *
 * @code
 * lcdWaitWhileBusy();
 * LCD_DB_DDR = 0xFF;	// DB as output
 * LCD_RS_RW_PORT |= LCD_RS_MASK;   // RS=1 (data)
 * LCD_RS_RW_PORT &= ~LCD_RW_MASK;  // RW=0 (write)
 * LCD_E_PORT |= LCD_E_MASK;        // E=1
 * LCD_DB_PORT = data;              // write data
 *
 * asm volatile ("nop");            // wait at least 140-60=80ns
 * asm volatile ("nop");            // (need 140 pulse width)
 *
 * LCD_E_PORT &= ~LCD_E_MASK;       // E=0
 *
 * asm volatile ("nop");            // wait at least 10ns
 * @endcode
 *
 * Reading busy flag:
 *
 * @code
 * LCD_DB_DDR = 0x00;  // DB as input
 * LCD_DB_PORT = 0xff; // Pull-up on DB port
 *
 * LCD_RS_RW_PORT &= ~LCD_RS_MASK; // RS=0 (instruction)
 * LCD_RS_RW_PORT |= LCD_RW_MASK;  // RW=1 (read)
 *
 * uint8_t state;
 *
 * // Check until busy flag is removed
 * do {
 *     LCD_E_PORT |= LCD_E_MASK;   // E=1
 *
 *     asm volatile ("nop");       // wait at least 100ns
 *     asm volatile ("nop");       // (data setup time)
 *
 *     state = LCD_DB_PIN;         // read state
 *
 *     LCD_E_PORT &= ~LCD_E_MASK;  // E=0
 *
 *     _delay_us(1); // wait 1000ns (for minimal cycle time of 1200ns)
 *
 * } while (state & LCD_BUSY_MASK);
 * @endcode
 */
#define LCD_E LCD_06

/**
 * Data bus pin 0
 *
 * Used for data transfer and receive between the MCU and LCD MCU.
 * This pin is not used during 4-bit operation.
 */
#define LCD_DB0          LCD_07

/**
 * Data bus pin 1
 *
 * Used for data transfer and receive between the MCU and LCD MCU.
 *
 * If #LCD_RW is in low (0) state, must be in output mode (#LCD_DB_DDR = 0xff).
 * If #LCD_RW is in high (1) state, must be in input mode with pull-up resistor
 * enabled (#LCD_DB_DDR = 0x00; #LCD_DB_PORT = 0xff;).
 *
 * This pin is not used during 4-bit operation.
 */
#define LCD_DB1 LCD_08

/**
 * Data bus pin 2
 *
 * Used for data transfer and receive between the MCU and LCD MCU.
 *
 * If #LCD_RW is in low (0) state, must be in output mode (#LCD_DB_DDR = 0xff).
 *
 * If #LCD_RW is in high (1) state, must be in input mode with pull-up resistor
 * enabled (#LCD_DB_DDR = 0x00; #LCD_DB_PORT = 0xff;).
 *
 * This pin is not used during 4-bit operation.
 */
#define LCD_DB2 LCD_09

/**
 * Data bus pin 3
 *
 * Used for data transfer and receive between the MCU and LCD MCU.
 *
 * If #LCD_RW is in low (0) state, must be in output mode (#LCD_DB_DDR = 0xff).
 *
 * If #LCD_RW is in high (1) state, must be in input mode with pull-up resistor
 * enabled (#LCD_DB_DDR = 0x00; #LCD_DB_PORT = 0xff;).
 *
 * This pin is not used during 4-bit operation.
 */
#define LCD_DB3 LCD_10

/**
 * Data bus pin 4
 *
 * Used for data transfer and receive between the MCU and LCD MCU.
 *
 * If #LCD_RW is in low (0) state, must be in output mode (#LCD_DB_DDR = 0xff).
 *
 * If #LCD_RW is in high (1) state, must be in input mode with pull-up resistor
 * enabled (#LCD_DB_DDR = 0x00; #LCD_DB_PORT = 0xff;).
 */
#define LCD_DB4 LCD_11

/**
 * Data bus pin 5
 *
 * Used for data transfer and receive between the MCU and LCD MCU.
 *
 * If #LCD_RW is in low (0) state, must be in output mode (#LCD_DB_DDR = 0xff).
 *
 * If #LCD_RW is in high (1) state, must be in input mode with pull-up resistor
 * enabled (#LCD_DB_DDR = 0x00; #LCD_DB_PORT = 0xff;).
 */
#define LCD_DB5 LCD_12

/**
 * Data bus pin 6
 *
 * Used for data transfer and receive between the MCU and LCD MCU.
 *
 * If #LCD_RW is in low (0) state, must be in output mode (#LCD_DB_DDR = 0xff).
 *
 * If #LCD_RW is in high (1) state, must be in input mode with pull-up resistor
 * enabled (#LCD_DB_DDR = 0x00; #LCD_DB_PORT = 0xff;).
 */
#define LCD_DB6 LCD_13

/**
 * Data bus pin 7
 *
 * Used for data transfer and receive between the MCU and LCD MCU.
 *
 * If #LCD_RW is in low (0) state, must be in output mode (#LCD_DB_DDR = 0xff).
 *
 * If #LCD_RW is in high (1) state, must be in input mode with pull-up resistor
 * enabled (#LCD_DB_DDR = 0x00; #LCD_DB_PORT = 0xff;).
 *
 * Can be used as busy flag (1 - busy, 0 - ready).
 */
#define LCD_DB7 LCD_14



/**
 * PORT register used by register select (RS) and read/write select (RW) pins
 */
#define  LCD_RS_RW_PORT  PORTD

/**
 * PIN register used by register select (RS) and read/write select (RW) pins
 */
#define  LCD_RS_RW_PIN   PIND

/**
 * DDR register used by register select (RS) and read/write select (RW) pins
 */
#define  LCD_RS_RW_DDR   DDRD

/**
 * PORT register used by start data read/write (E) pin
 */
#define  LCD_E_PORT      PORTE

/**
 * PIN register used by start data read/write (E) pin
 */
#define  LCD_E_PIN       PINE

/**
 * DDR register used by start data read/write (E) pin
 */
#define  LCD_E_DDR       DDRE

/**
 * PORT register used by data bus (DB7-DB0) pins
 */
#define  LCD_DB_PORT     PORTC

/**
 * PIN register used by data bus (DB7-DB0) pins
 */
#define  LCD_DB_PIN      PINC

/**
 * DDR register used by data bus (DB7-DB0) pins
 */
#define  LCD_DB_DDR      DDRC



/**
 * Register select pin mask, see #LCD_RS for details
 */
static const uint8_t LCD_RS_MASK  = (1<<LCD_RS );

/**
 * Read/write select pin mask, see #LCD_RW for details
 */
static const uint8_t LCD_RW_MASK  = (1<<LCD_RW );

/**
 * Data read/write start/stop pin mask, see #LCD_E for details
 */
static const uint8_t LCD_E_MASK   = (1<<LCD_E  );

/**
 * Data bus pin 0 mask, see #LCD_DB0 for details
 */
static const uint8_t LCD_DB0_MASK = (1<<LCD_DB0);
/**
 * Data bus pin 1 mask, see #LCD_DB1 for details
 */
static const uint8_t LCD_DB1_MASK = (1<<LCD_DB1);

/**
 * Data bus pin 2 mask, see #LCD_DB2 for details
 */
static const uint8_t LCD_DB2_MASK = (1<<LCD_DB2);

/**
 * Data bus pin 3 mask, see #LCD_DB3 for details
 */
static const uint8_t LCD_DB3_MASK = (1<<LCD_DB3);

/**
 * Data bus pin 4 mask, see #LCD_DB4 for details
 */
static const uint8_t LCD_DB4_MASK = (1<<LCD_DB4);

/**
 * Data bus pin 5 mask, see #LCD_DB5 for details
 */
static const uint8_t LCD_DB5_MASK = (1<<LCD_DB5);

/**
 * Data bus pin 6 mask, see #LCD_DB6 for details
 */
static const uint8_t LCD_DB6_MASK = (1<<LCD_DB6);

/**
 * Data bus pin 7 mask, see #LCD_DB7 for details
 */
static const uint8_t LCD_DB7_MASK = (1<<LCD_DB7);



/*********
 * Masks *
 *********/

/**
 * Busy flag mask
 *
 * Should be used when checking if LCD MCU is busy. High state (1) indicates
 * that LCD MCU is busy, low (0) indicates that it is ready to accept new
 * instructions.
 */
static const uint8_t LCD_BUSY_MASK = 0x01;



/**
 * Clear Display instruction mask
 *
 * Write 0x20 to DDRAM and set DDRAM address to 0x00 from AC.
 *
 * Note that description time for this instruction is 1.52ms (@270KHz)
 *
 * Used in call to lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_CLEAR_DISPLAY = 0x80;



/**
 * Return Home instruction mask
 *
 * Set DDRAM address to 0x00 from AC and return cursor to its original position
 * if shifted. The contents of DDRAM are not changed.
 *
 * Note that description time for this instruction is 1.52ms (@270KHz)
 *
 * Used in call to lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_RETURN_HOME = 0x40;



/**
 * Entry Mode Set instruction mask
 *
 * Controls display entry mode. Sets cursor move direction and specifies
 * display shift. These operations are performed during data write and read.
 *
 * Can be used with #LCD_ENTRY_MODE_SET_SHIFT_RIGHT
 * and #LCD_ENTRY_MODE_SET_SHIFT_DISPLAY in call to
 * lcdWriteInstrRev(uint8_t instr).
 *
 * Note that description time for this instruction is 37us (@270KHz)
 */
static const uint8_t LCD_ENTRY_MODE_SET_INSTR = 0x20;

/**
 * Entry Mode Set direction mask
 *
 * When used, cursor/blinker moves to right and DDRAM address is increased by 1.
 * When not used, cursor/blinker moves to left and DDRAM address is decreased
 * by 1.
 *
 * Should be used with #LCD_ENTRY_MODE_SET_INSTR in call to
 * lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_ENTRY_MODE_SET_SHIFT_RIGHT = 0x40;

/**
 * Entry Mode Set display shift mask
 *
 * When used, on DDRAM write operation, shift of entire display is performed
 * according to #LCD_ENTRY_MODE_SET_SHIFT_RIGHT value.
 * When not used, shift of entire display is not performed.
 *
 * Should be used with #LCD_ENTRY_MODE_SET_INSTR in call to
 * lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_ENTRY_MODE_SET_SHIFT_DISPLAY = 0x80;



/**
 * LCD on/off instruction mask
 *
 * Controls display state and cursor/blinker behavior (on/off).
 * Can be used with #LCD_DISPLAY_ON, #LCD_CURSOR_ON and #LCD_BLINKER_ON in
 * call to lcdWriteInstrRev(uint8_t instr).
 *
 * Note that description time for this instruction is 37us (@270KHz)
 */
static const uint8_t LCD_ON_OFF_INSTR = 0x10;

/**
 * Display ON mask
 *
 * When used, LCD display will be enabled. When not used, LCD will not display
 * anything.
 *
 * Should be used with #LCD_ON_OFF_INSTR in call to
 * lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_DISPLAY_ON = 0x20;

/**
 * Cursor ON mask
 *
 * When used, cursor is turned on. When not used, cursor will not be displayed.
 * Cursor is displayed as solid black line in the last row of the symbol.
 *
 * Note that this option is independent from #LCD_BLINKER_ON.
 *
 * Should be used with #LCD_ON_OFF_INSTR in call to
 * lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_CURSOR_ON = 0x40;

/**
 * Blinker mask
 *
 * When used, position of cursor will blink (symbol will go to solid black and
 * then back to symbol bitmap).
 * When not used, cursor position will not blink.
 *
 * Note that this option is independent from #LCD_CURSOR_ON.
 *
 * Should be used with #LCD_ON_OFF_INSTR in call to
 * lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_BLINKER_ON = 0x80;



/**
 * Cursor or display shift instruction mask
 *
 * Shifts cursor or display position without writing or reading data.
 *
 * Notes:
 * * If 2-line mode is set, cursor moves to the 2nd line after 40th digit
 *   of the 1st line.
 * * Note that the first line end address and the second line start address are
 *   not consecutive (symbols in the 1st line are in range from 0x00 to 0x27,
 *   and for the 2nd line are in range from 0x40 to 0x67).
 * * Display shift is performed simultaneously in all lines.
 * * When displayed data is shifted repeatedly, each line is
 *   shifted individually.
 * * When display shift is performed, the contents of address counter
 *   are not changed.
 *
 * Can be used with #LCD_SHIFT_DISPLAY and #LCD_SHIFT_RIGHT in call to
 * lcdWriteInstrRev(uint8_t instr).
 *
 * Note that description time for this instruction is 37us (@270KHz)
 */
static const uint8_t LCD_CURSOR_OR_DISPLAY_SHIFT_INSTR = 0x08;

/**
 * Shift display instead of cursor instruction mask
 *
 * When used, entire display will be shifted. Cursor follows display shift.
 * When not used, only cursor will be shifted.
 *
 * Should be used with #LCD_CURSOR_OR_DISPLAY_SHIFT_INSTR in call to
 * lcdWriteInstrRev(uint8_t instr).
 *
 * @see #LCD_SHIFT_RIGHT - choose whether left or roght shift is used.
 */
static const uint8_t LCD_SHIFT_DISPLAY = 0x10;

/**
 * Shift cursor/display to the right instead of left
 *
 * When used, cursor/display will be shifted right.
 * When not used, cursor/display will be shifted left.
 *
 * Should be used with #LCD_CURSOR_OR_DISPLAY_SHIFT_INSTR in call to
 * lcdWriteInstrRev(uint8_t instr).
 *
 * @see #LCD_SHIFT_DISPLAY - choose whether cursor or display gets shifted.
 */
static const uint8_t LCD_SHIFT_RIGHT = 0x20;



/**
 * Function set instruction mask
 *
 * Function set instruction controls type of display used (data bus width,
 * number of lines and character size). Can be used with
 * #LCD_FUNCTION_SET_8BIT, #LCD_FUNCTION_SET_2LINE and #LCD_FUNCTION_SET_5X11.
 *
 * Note that description time for this instruction is 37us (@270KHz).
 *
 * Used by lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_FUNCTION_SET_INSTR = 0x04;

/**
 * Function set 8-bit data bus mask
 *
 * When used, 8-bit data bus is used (all pins DB0 - DB7 are used for read/write
 * operations).
 * When not used, display uses 4-bit data bus (only pins DB4 - DB7).
 *
 * Should be used with #LCD_FUNCTION_SET_INSTR.
 *
 * Used by lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_FUNCTION_SET_8BIT = 0x08;

/**
 * Function set 2 line display mask
 *
 * When used, display MCU uses two display lines.
 * When not used, only one line is used.
 *
 * Should be used with #LCD_FUNCTION_SET_INSTR.
 *
 * Used by lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_FUNCTION_SET_2LINE = 0x10;

/**
 * Function set font selection mask
 *
 * When used, 5x11 pixel character font is used.
 * Otherwise, 5x8 pixel font is used instead.
 *
 * Should be used with #LCD_FUNCTION_SET_INSTR.
 *
 * Used by lcdWriteInstrRev(uint8_t instr).
 */
static const uint8_t LCD_FUNCTION_SET_5X11 = 0x20;



/**
 * Set CGRAM address instruction mask
 *
 * This instruction makes CGRAM data available from MCU.
 * Subsequent lcdWriteData() and lcdWriteDataRev() operations will write to
 * CGRAM and lcdReadData() and lcdReadDataRev() will read data from CGRAM,
 * beginning at specified address.
 *
 * Bits DB5-DB0 are used to set CGRAM address (AC5-AC0). Note that for
 * UT DEVBoard these should be given in reverse order
 * ([AC0, AC1, AC2, AC3, AC4, AC5, INSTR, 0]).
 * You can use lcdReverseBits(uint8_t data) to convert conventional address
 * to appropriate reversed form. Also note that data written to both CGRAM
 * and DDRAM also has to be reversed.
 *
 * To switch back to writing to DDRAM, lcdWriteInstrRev() must be
 * called for #LCD_SET_DDRAM_ADDR_INSTR.
 *
 * Note that description time for this instruction is 37us (@270KHz).
 *
 * Used by lcdWriteInstrRev(uint8_t instr).
 *
 * Example:
 *
 * @code
 * // display must be initialized before this code is executed
 * uint8_t ddramAddr = 0;
 * uint8_t cgramAddr = 0;
 * uint8_t cgramData = 0b00010101;
 * // switch to CGRAM access
 * lcdWriteInstrRev(LCD_SET_CGRAM_ADDR_INSTR | lcdReverseBits(uint8_t cgramAddr));
 * // write data to CGRAM
 * lcdWriteData(lcdReverseBits(uint8_t cgramData));
 * // switch back to DDRAM access
 * lcdWriteInstrRev(LCD_SET_DDRAM_ADDR_INSTR | lcdReverseBits(uint8_t ddramAddr));
 * @endcode
 *
 * This code will write cgramData to CGRAM at cgramAddr and then switch back
 * to DDRAM at ddramAddr.
 *
 * @see #LCD_SET_DDRAM_ADDR_INSTR
 * @see lcdWriteInstrRev(uint8_t instr)
 * @see lcdReverseBits(uint8_t data)
 */
static const uint8_t LCD_SET_CGRAM_ADDR_INSTR = 0x02;

/**
 * Set DDRAM address instruction mask
 *
 * This instruction makes DDRAM data available from MCU.
 * Subsequent lcdWriteData() and lcdWriteDataRev() operations will write to
 * DDRAM and lcdReadData() and lcdReadDataRev() will read data from DDRAM,
 * beginning at specified address.
 *
 * When 1-line display mode is used, valid DDRAM addresses are in range
 * from 0x00 to 0x4f. If 2-line display mode is used, DDRAM addresses for
 * symbols in the 1st line are in range from 0x00 to 0x27 (incl), and for
 * the 2nd line are in range from 0x40 to 0x67 (incl).
 *
 * Subsequent lcdWriteData() operations will write to DDRAM,
 * beginning at specified address.
 *
 * Bits DB6-DB0 are used to set DDRAM address (AC6-AC0). Note that for
 * UT DEVBoard these should be given in reverse order
 * ([AC0, AC1, AC2, AC3, AC4, AC5, AC6, INSTR]).
 * You can use lcdReverseBits(uint8_t data) to convert conventional address
 * to appropriate reversed form. Also note that data written to both DDRAM
 * and CGRAM also has to be reversed.
 *
 * Note that description time for this instruction is 37us (@270KHz).
 *
 * Used by lcdWriteInstrRev(uint8_t instr).
 *
 * Example:
 *
 * @code
 * // display must be initialized before this code is executed
 * uint8_t ddramAddr = 0;
 * uint8_t ddramData = 'X';
 * // switch back to DDRAM access at ddramAddr
 * lcdWriteInstrRev(LCD_SET_DDRAM_ADDR_INSTR | lcdReverseBits(uint8_t ddramAddr));
 * lcdWrideData(lcdReverseBits(uint8_t ddramData));
 * @endcode
 *
 * This code will write ddramData (character 'X') to DDRAM at ddramAddr.
 *
 * @see #LCD_SET_CGRAM_ADDR_INSTR
 * @see #LCD_FIRST_LINE_START_ADDR
 * @see #LCD_SECOND_LINE_START_ADDR
 * @see #LCD_SECOND_LINE_START_ADDR_REV
 * @see lcdWriteInstrRev(uint8_t instr)
 * @see lcdReverseBits(uint8_t data)
 */
static const uint8_t LCD_SET_DDRAM_ADDR_INSTR = 0x01;



/**
 * Address of first symbol of the first line in 2-line mode (must be reversed)
 */
static const uint8_t LCD_FIRST_LINE_START_ADDR = 0x00;

/**
 * Address of first symbol of the first line in 2-line mode (reversed)
 */
static const uint8_t LCD_FIRST_LINE_START_ADDR_REV = 0x00;

/**
 * Address of first symbol of the second line in 2-line mode (must be reversed)
 */
static const uint8_t LCD_SECOND_LINE_START_ADDR = 0x40;

/**
 * Address of first symbol of the second line in 2-line mode (reversed)
 */
static const uint8_t LCD_SECOND_LINE_START_ADDR_REV = 0x02;

/**
 * Address of first symbol in single line mode (must be reversed)
 */
static const uint8_t LCD_SINGLE_LINE_START_ADDR = 0x00;

/**
 * Address of first symbol in single line mode (reversed)
 */
static const uint8_t LCD_SINGLE_LINE_START_ADDR_REV = 0x00;

/**
 * Address immediately after the last symbol of the first line in 2-line mode
 * (must be reversed)
 */
static const uint8_t LCD_FIRST_LINE_END_ADDR = 0x28;

/**
 * Address immediately after the last symbol of the first line in 2-line mode
 * (reversed)
 */
static const uint8_t LCD_FIRST_LINE_END_ADDR_REV = 0x14;

/**
 * Address immediately after the last symbol of the second line in 2-line mode
 * (must be reversed)
 */
static const uint8_t LCD_SECOND_LINE_END_ADDR = 0x68;

/**
 * Address immediately after the last symbol of the second line in 2-line mode
 * (reversed)
 */
static const uint8_t LCD_SECOND_LINE_END_ADDR_REV = 0x16;

/**
 * Address immediately after the last symbol in 2-line mode (must be reversed)
 */
static const uint8_t LCD_SINGLE_LINE_END_ADDR = 0x4f;

/**
 * Address immediately after the last symbol in 2-line mode (reversed)
 */
static const uint8_t LCD_SINGLE_LINE_END_ADDR_REV = 0xf2;

/**
 * Maximum available number of symbols in single line mode
 *
 * #LCD_SINGLE_LINE_START_ADDR + #LCD_SINGLE_LINE_LENGTH gives
 * position immediately after the end of DDRAM area used to store characters
 */
static const uint8_t LCD_SINGLE_LINE_LENGTH = 0x4f;

/**
 * Maximum available number of symbols per line in 2-line mode
 *
 * #LCD_FIRST_LINE_START_ADDR + #LCD_TWO_LINE_LENGTH gives
 * position immediately after the end of DDRAM area used to store characters of
 * the first line, #LCD_SECOND_LINE_START_ADDR + #LCD_TWO_LINE_LENGTH gives
 * position immediately after the end of DDRAM area used to store characters of
 * the second line.
 */
static const uint8_t LCD_TWO_LINE_LENGTH = 0x27;



/*************
 * Functions *
 *************/


/**
 * Initialize ports used by display
 *
 * Sets up RS, RW and E pin ports as outputs. Should be called before any other
 * function from this file.
 */
void lcdInitPorts();


/**
 * Initialize display
 *
 * Enables and sets up the display. Should be called after lcdInitPorts() and
 * before any other function from this file.
 *
 * This function can be used only after 40ms has passed since device was powered
 * on. Alternatively, lcdInitInstrWithDelay() can be used instead if
 * initialization must happen immediately after device boot.
 */
void lcdInitInstr();


/**
 * Wait 40ms and initialize display
 *
 * Enables and sets up the display. Should be called after lcdInitPorts() and
 * before any other function from this file.
 *
 * Useful when trying to initialize display immediately after power-on to
 * ensure that display MCU has finished booting up.
 *
 * Alternatively, lcdInitInstr() can be used instead if 40ms has definitely
 * passed from the moment when Vcc was applied to display MCU.
 */
void lcdInitInstrWithDelay();


/**
 * Wait until display MCU is ready to accept another instruction
 *
 * Does not return until display busy flag is cleared. If display was not set up
 * properly (by calling lcdInitPorts() and lcdInitInstrWithDelay() functions),
 * it will most likely hang in eternal loop.
 */
void lcdWaitWhileBusy();


/**
 * Send instruction to LCD display MCU without checking the busy flag
 *
 * This instruction should be used only during display initialization when busy
 * flag is not available. lcdWriteInstrRev(uint8_t instr) should be used instead in
 * any other case.
 *
 * @param instr - instruction that will be sent to display MCU
 *
 * NOTE: On University of Tartu AT90USB647 DEVBoard instructions
 * SHOULD BE REVERSED compared to ST7066U data sheet!
 * Use masks defined in this file to avoid having to define them manually.
 *
 * Possible instructions (see constant description for details):
 * * #LCD_CLEAR_DISPLAY
 * * #LCD_RETURN_HOME
 * * #LCD_ENTRY_MODE_SET_INSTR (with parameters)
 * * #LCD_ON_OFF_INSTR (with parameters)
 * * #LCD_FUNCTION_SET_INSTR (with parameters)
 * * #LCD_SET_CGRAM_ADDR_INSTR (see description for data format)
 * * #LCD_SET_DDRAM_ADDR_INSTR (see description for data format)
 */
void lcdWriteInstrNoCheckRev(const uint8_t instr);


/**
 * Wait for busy flag to be cleared and send instruction to LCD display MCU
 *
 * During initialization when busy flag is not available,
 * lcdWriteInstrNoCheckRev(uint8_t instr) can be used instead.
 *
 * @param instr - instruction that will be sent to display MCU
 *
 * NOTE: On University of Tartu AT90USB647 DEVBoard instructions
 * SHOULD BE REVERSED compared to ST7066U data sheet!
 * Use masks defined in this file to avoid having to define them manually.
 *
 * Possible instructions (see constant description for details):
 * * #LCD_CLEAR_DISPLAY
 * * #LCD_RETURN_HOME
 * * #LCD_ENTRY_MODE_SET_INSTR (with parameters)
 * * #LCD_ON_OFF_INSTR (with parameters)
 * * #LCD_FUNCTION_SET_INSTR (with parameters)
 * * #LCD_SET_CGRAM_ADDR_INSTR (see description for data format)
 * * #LCD_SET_DDRAM_ADDR_INSTR (see description for data format)
 */
void lcdWriteInstrRev(const uint8_t instr);


/**
 * Wait for busy flag to be cleared and send data to LCD display MCU
 *
 * @param data - data that will be sent to display MCU
 *
 * NOTE: On University of Tartu AT90USB647 DEVBoard data
 * SHOULD BE REVERSED compared to ST7066U data sheet!
 * Consider using lcdWriteData() instead.
 *
 * @see lcdWriteData() - reversing version of this function
 */
void lcdWriteDataRev(const uint8_t data);


/**
 * Wait for busy flag to be cleared and send (reversed) data to LCD display MCU
 *
 * This function reverses bits in data before sending them to display.
 * This means that it works with data as defined in ST7066U data sheet.
 *
 * @param data - data that will be sent to display MCU
 *
 * @see lcdWriteDataRev() - non-reversing version of this function
 */
void lcdWriteData(uint8_t data);


/**
 * Wait for busy flag to be cleared and read 8-bit data from LCD DDRAM/CGRAM
 *
 * RAM address and type is set by the previous address set instruction
 * (see #LCD_SET_DDRAM_ADDR_INSTR and # LCD_SET_CGRAM_ADDR_INSTR).
 *
 * If address set instruction was not performed before this instruction,
 * the first read is invalid. If RAM data will be read several times without
 * address set instruction, second read will be valid.
 *
 * For DDRAM read operation, cursor shift instruction also serves as
 * DDRAM address set instruction. After read operation is performed,
 * address counter is automatically increased/decreased by 1 according to
 * the entry mode (see #LCD_ENTRY_MODE_SET_INSTR).
 *
 * **NB! After CGRAM read operation, display shift may not be executed
 * correctly!**
 *
 * **Note that after RAM write operation (see lcdWriteData(), lcdWriteDataRev()
 * and lcdWriteInstrRev()), AC will be increased/decreased by 1 similarly to
 * read operation, but, although AC will point to the next/previous address
 * position, only data that AC previously pointed to will be read by this
 * function.**
 *
 * @returns uint8_t that was read from LCD DDRAM/CGRAM
 *
 * NOTE: On University of Tartu AT90USB647 DEVBoard read data
 * WILL BE REVERSED compared to ST7066U data sheet!
 * Consider using lcdReadData() instead.
 *
 * @see lcdReadData() - reversing version of this function
 */
uint8_t lcdReadDataRev();


/**
 * Wait for busy flag to be cleared and read 8-bit data from LCD DDRAM/CGRAM
 *
 * RAM address and type is set by the previous address set instruction
 * (see #LCD_SET_DDRAM_ADDR_INSTR and # LCD_SET_CGRAM_ADDR_INSTR).
 *
 * If address set instruction was not performed before this instruction,
 * the first read is invalid. If RAM data will be read several times without
 * address set instruction, second read will be valid.
 *
 * For DDRAM read operation, cursor shift instruction also serves as
 * DDRAM address set instruction. After read operation is performed,
 * address counter is automatically increased/decreased by 1 according to
 * the entry mode (see #LCD_ENTRY_MODE_SET_INSTR).
 *
 * **NB! After CGRAM read operation, display shift may not be executed
 * correctly!**
 *
 * **Note that after RAM write operation (see lcdWriteData(), lcdWriteDataRev()
 * and lcdWriteInstrRev()), AC will be increased/decreased by 1 similarly to
 * read operation, but, although AC will point to the next/previous address
 * position, only data that AC previously pointed to will be read by this
 * function.**
 *
 * This function reverses bits in data before returning them.
 * This means that it works with data as defined in ST7066U data sheet.
 *
 * @returns uint8_t that was read from LCD DDRAM/CGRAM (reversed)
 *
 * @see lcdReadDataRev() - non-reversing version of this function
 */
uint8_t lcdReadData();


/**
 * Reverse order of bits in given uint8_t
 *
 * This function is needed to convert instructions and data into format used
 * by the given development board without using huge lookup tables.
 *
 * @param data - initial value
 * @return uint8_t with reversed order of bits
 */
uint8_t lcdReverseBits(uint8_t data);


#endif /* _LCD_IO_CORE_H_ */
