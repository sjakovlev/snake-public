#ifndef _SNAKE_DISPLAY_H_
#define _SNAKE_DISPLAY_H_

#include <stdint.h>

void snakeDisplayInit(uint8_t width, uint8_t height);

void snakeDisplayPutPixel(uint8_t x, uint8_t y);

void snakeDisplayRemovePixel(uint8_t x, uint8_t y);

void snakeDisplayClear();

#endif /* _SNAKE_DISPLAY_H_ */
