/** @file snake_field.h
 *
 * Snake field declarations
 *
 * This file contains declarations of constants and functions that store snake
 * state and allow to modify it.
 *
 * @author Sergei Jakovlev
 * @author Anton Prokopov
 */

#ifndef _SNAKE_FIELD_H_
#define _SNAKE_FIELD_H_

#include <stdint.h>


/*
 * Constants
 */


/**
 * Maximum allowed length of the snake
 *
 * Snake will not grow beyond this length.
 */
#define SNAKE_MAX_LEN 64



/**
 * Direction value "above the previous one" (see #snakeSegments)
 *
 * Opposite direction is ~SNAKE_UP == #SNAKE_DOWN.
 */
static const uint8_t SNAKE_UP = 0x01;	// 0b0000 0001

/**
 * Direction value "below the previous one" (see #snakeSegments)
 *
 * Opposite direction is ~SNAKE_DOWN == #SNAKE_UP.
 */
static const uint8_t SNAKE_DOWN = 0xfe;	// 0b1111 1110

/**
 * Direction value "left of the previous one" (see #snakeSegments)
 *
 * Opposite direction is ~SNAKE_LEFT == #SNAKE_RIGHT.
 */
static const uint8_t SNAKE_LEFT = 0x02;	// 0b0000 0010

/**
 * Direction value "right of the previous one" (see #snakeSegments)
 *
 * Opposite direction is ~SNAKE_RIGHT == #SNAKE_LEFT.
 */
static const uint8_t SNAKE_RIGHT= 0xfd;	// 0b1111 1101


/*
 * Functions
 */


/**
 * Initialize field of with given dimensions
 *
 * Initializes internal variables, data structures and display.
 *
 * @param fieldWidth width of the field
 * @param fieldHeight height of the field
 *
 * @see snakeInit - initializes snake (snakeFieldInit initializes empty field)
 */
void snakeFieldInit(uint8_t fieldWidth, uint8_t fieldHeight);



/**
 * Reset snake state and place 1 segment long snake at (x, y)
 *
 * @param x horizontal coordinate of snake's head after reset
 * @param y vertical coordinate of snake's head after reset
 */
void snakeInit(uint8_t x, uint8_t y);



/**
 * Set whether the field should be wrapped (snake can pass "through" borders)
 *
 * @param isWrapped if 0, collisions with borders will end a game; otherwise
 * when moving out of screen snake will appear on the other side of the screen.
 */
void snakeSetIsFieldWrapped(uint8_t isWrapped);

/**
 * Get whether the field is wrapped (snake can pass "through" borders)
 *
 * @return 0 if collisions with borders will end a game; otherwise
 * when moving out of screen snake will appear on the other side of the screen.
 */
uint8_t snakeGetIsFieldWrapped();



/**
 * Clear display and forcefully redraw all segments of the snake
 *
 * Does not modify the state of the snake and simply clears display
 * and causes each active segment of the snake to be drawn anew.
 *
 * Since snakeEat() and snakeStep() functions update only changed fragments
 * without redrawing anything else, if something besides this field was
 * displayed on the screen (e.g. some text) this method must be called to redraw
 * the entire field at once.
 */
void snakeRedrawAll();



/**
 * Move snake's head one step in the given direction and "drag" the body behind
 *
 * When using this function length of the snake does not change.
 *
 * Note that this function checks only for collisions with snake's head and
 * ignores other segments of the snake.
 *
 * @param direction one of #SNAKE_UP, #SNAKE_DOWN, #SNAKE_LEFT or #SNAKE_RIGHT
 *
 * @return 1 if snake collided with field borders or itself; 0 otherwise
 *
 * @see snakeEat()
 */
uint8_t snakeStep(uint8_t direction);

/**
 * Move snake's head one step in the given direction by adding a new segment
 *
 * When using this function snake becomes longer by one segment until reaching
 * maximal length (see #SNAKE_MAX_LEN). When maximal length is reached, behaves
 * identically to snakeStep() called with the same argument.
 *
 * Note that this function checks only for collisions with snake's head and
 * ignores other segments of the snake.
 *
 * @param direction #SNAKE_UP, #SNAKE_DOWN, #SNAKE_LEFT or #SNAKE_RIGHT
 *
 * @return 1 if snake's head collided with field borders or itself; 0 otherwise
 *
 * @see snakeStep()
 */
uint8_t snakeEat(uint8_t direction);



/**
 * Check if given position is occupied by some snake segment
 *
 * @param x horizontal coordinate
 * @param y vertical coordinate
 *
 * @return 0 if position is free (not occupied by some snake segment; it may
 * still contain food); 1 otherwise
 */
uint8_t snakeIsPositionTaken(uint8_t x, uint8_t y);



/**
 * Put food point to the random spot in the snake field.
 * Check if any of the snake points collides with the food point.
 * If it does, find another spot to put food in.
 * Otherwise, put the food point to the spot.
 *
 * @return 1 if the new spot for food point was found, 0 if there are no spawn spots left (win state)
 */
uint8_t spawnFood();



/**
 * Return x coordinate of food
 *
 * @return uint8_t food point x coordinate
 */
uint8_t getFoodX();

/**
 * Return y coordinate of food
 *
 * @return uint8_t food point y coordinate
 */
uint8_t getFoodY();

/**
 * Return x coordinate of snake's head
 *
 * @return uint8_t snake's head x coordinate
 */
uint8_t getHeadX();

/**
 * Return y coordinate of snake's head
 *
 * @return uint8_t snake's head y coordinate
 */
uint8_t getHeadY();

/**
 * Return snake's length
 *
 * Length is calculated by calculating the difference between snake's
 * head and tail indexes.
 *
 * @return uint8_t snake's length
 */
uint8_t getSnakeLength();



#endif /* _SNAKE_FIELD_H_ */
