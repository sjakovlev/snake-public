############################
# "Snake" project makefile #
#                          #
# Author: Sergei Jakovlev  #
# Date:   19.06.2015       #
############################


###########
# Options #
###########

CC := avr-gcc
CFLAGS := 

LD := avr-gcc
LDFLAGS := 

SRC_DIR := src
HEADER_DIR := include
BUILD_DIR := build
DOCS_DIR := docs

FLASH_MCU_TARGET := at90usb647
AVRDUDE_TARGET := usb647

#SOURCES := $(wildcard $(SRC_DIR)/*.c)
SOURCES := $(SRC_DIR)/snake_field.c $(SRC_DIR)/snake_display_lcd.c $(SRC_DIR)/lcd_io_core.c $(SRC_DIR)/lcd_io.c $(SRC_DIR)/snake_main.c
OBJECTS := $(SOURCES:$(SRC_DIR)/%.c=$(BUILD_DIR)/%.o)

PROJECT_NAME := snake
PROJECT_OBJ := $(BUILD_DIR)/$(PROJECT_NAME).o
PROJECT_HEX := $(BUILD_DIR)/$(PROJECT_NAME).hex



#################
# Build targets #
#################

all: $(PROJECT_HEX)

$(PROJECT_HEX): $(PROJECT_OBJ)
	avr-objcopy -j .text -j .data -O ihex $< $@

$(PROJECT_OBJ): $(OBJECTS)
	$(LD) $(LDFLAGS) -mmcu=$(FLASH_MCU_TARGET) -std=gnu11 \
	-Wl,--gc-sections \
	-Wall -Wextra -Wpedantic \
	-I../include -I$(HEADER_DIR) \
	$(OBJECTS) \
	-o $@

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c
	$(CC) $(CFLAGS) -c -mmcu=$(FLASH_MCU_TARGET) -std=gnu11 \
	-Os -DF_CPU=2000000UL -ffunction-sections -fdata-sections \
	-Wall -Wextra -Wpedantic \
	-I../include -I$(HEADER_DIR) \
	$< \
	-o $@



#####################
# Auxillary targets #
#####################

docs:
	doxygen

flash: $(PROJECT_HEX)
	avrdude -c flip1 -p $(AVRDUDE_TARGET) -U flash:w:$(PROJECT_HEX)

clean:
	rm -f $(OBJECTS) $(PROJECT_OBJ) $(PROJECT_HEX) $(TEST_OBJECTS) $(TEST_PROJECT_OBJ)
	rm -rf $(DOCS_DIR)



.PHONY: clean flash docs
